$(function(){
  
  $("img.large").click(function(){
    var $body = $('body');
    $(this).addClass("current"); 
    $image = $(this)[0].src;
    
    var $blackout = $("<div id='blackout'>").css("display", "none");
    
    var $img = $("<img>").attr("src", $image);
    $blackout.append($img);
    $blackout.append('<i class="blackout-close fa fa-3x fa-close"></i>');
    $blackout.append('<i class="blackout-prec fa fa-3x fa-chevron-left"></i>');
    $blackout.append('<i class="blackout-next fa fa-3x fa-chevron-right"></i>');

    $body.append($blackout);
    $blackout.fadeIn();
    
    if($img.height() < $blackout.height()){
      $img.css("marginTop", ($blackout.height() - $img.height()) / 2);
    }
  });

  $("img.img_carousel").click(function(){
    var $body = $('body');
    $(this).addClass("current"); 
    $image = $(this)[0].src;
    
    var $blackout = $("<div id='blackout'>").css("display", "none");
    
    var $img = $("<img>").attr("src", $image);
    $blackout.append($img);
    $blackout.append('<i class="blackout-close fa fa-3x fa-close"></i>');

    $body.append($blackout);
    $blackout.fadeIn();
    
    if($img.height() < $blackout.height()){
      $img.css("marginTop", ($blackout.height() - $img.height()) / 2);
    }
  });
  
  $(document).on("click", ".blackout-close", function(){
    $current = $(".current");
    $current.removeClass("current");
    $("#blackout").fadeOut(function(){
      $("#blackout").remove();
    });
  });

  $(document).on("click", ".blackout-prec", function(){
    $current = $(".current");
    if($current.prev()[0] != undefined){
      $current.removeClass("current");
      $current = $current.prev();
      $current.addClass("current");
      $image = $current[0].src;
      $("#blackout>img").remove();
      $img = $("<img>").attr("src", $image);
      $("#blackout").append($img);

      if($img.height() < $("#blackout").height()){
        $img.css("marginTop", ($("#blackout").height() - $img.height()) / 2);
      }
    }
  });

  $(document).on("click", ".blackout-next", function(){
    $current = $(".current");
    if($current.next()[0] != undefined){
      $current.removeClass("current");
      $current = $current.next();
      $current.addClass("current");
      $image = $current[0].src;
      $("#blackout>img").remove();
      $img = $("<img>").attr("src", $image);
      $("#blackout").append($img);

      if($img.height() < $("#blackout").height()){
        $img.css("marginTop", ($("#blackout").height() - $img.height()) / 2);
      }
    }
  });

  $(".plaquette>a").click(function(){
    var $body = $('body');
    $(this).addClass("current"); 
    $image = $(this).data("src");

    var $blackout = $("<div id='blackout'>").css("display", "none");
    
    var $img = $("<img>").attr("src", $image);
    $blackout.append($img);
    $blackout.append('<i class="blackout-close fa fa-3x fa-close"></i>');

    $body.append($blackout);
    $blackout.fadeIn();
    
    $img.on('load', function(){
      if($(this).height() < $blackout.height()){
        $(this).css("marginTop", ($blackout.height() - $(this).height()) / 2);
      }else {
        $(this)[0].style.height = $blackout.height()-200 + "px"
        $(this).css("marginTop", ($blackout.height() - $(this).height()) / 2);
      }
    });
  });

});