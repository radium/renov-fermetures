var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var flash = require('connect-flash');
var nodemailer = require('nodemailer');

var app = express();
app.use(cookieParser());
var transporter = nodemailer.createTransport();

//Settings
app.set('views', __dirname + '/views');
app.set('view engine','ejs');
app.use('/public', express.static(__dirname + '/public'));

app.use(cookieParser('keyboard cat'));
app.use(session({ 
  cookie: { maxAge: 60000 },
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}));
app.use(flash());

app.use(bodyParser.urlencoded({ extended: false }))

//Routing
//app.get('*', function(req, res){
//  res.render('maintenance');
//});

app.get('/', function (req, res) {
  res.render('index', { ok: req.flash('info'), no: "" });
});
app.get('/bois', function (req, res) {
  res.render('bois', { ok: "", no: "" });
});
app.get('/bois/fenetres', function (req, res) {
  res.render('fenetres-bois', { ok: "", no: "" });
});
app.get('/bois/portes', function (req, res) {
  res.render('portes-bois', { ok: "", no: "" });
});
app.get('/bois/volets', function (req, res) {
  res.render('volets-bois', { ok: "", no: "" });
});
app.get('/pvc', function (req, res) {
  res.render('pvc', { ok: "", no: "" });
});
app.get('/pvc/fenetres', function (req, res) {
  res.render('fenetres-pvc', { ok: "", no: "" });
});
app.get('/pvc/portes-entree', function (req, res) {
  res.render('portes-pvc', { ok: "", no: "" });
});
app.get('/pvc/volets', function (req, res) {
  res.render('volets-pvc', { ok: "", no: "" });
});
app.get('/alu', function (req, res) {
  res.render('alu', { ok: "", no: "" });
});
app.get('/alu/baies-vitrees', function (req, res) {
  res.render('baies-vitrees-alu', { ok: "", no: "" });
});
app.get('/alu/portes-entree', function (req, res) {
  res.render('portes-alu', { ok: "", no: "" });
});
app.get('/alu/volets', function (req, res) {
  res.render('volets-alu', { ok: "", no: "" });
});
app.get('/alu/portes-garage', function (req, res) {
  res.render('portes-garage-alu', { ok: "", no: "" });
});
app.get('/acier', function (req, res) {
  res.render('acier', { ok: "", no: "" });
});
app.get('/acier/portes-garage', function (req, res) {
  res.render('portes-garage-acier', { ok: "", no: "" });
});
app.get('/acier/portails', function (req, res) {
  res.render('portails-acier', { ok: "", no: "" });
});
app.get('/contact', function (req, res) {
  res.render('contact', { no: req.flash('info'), ok: "" });
});
app.get('/mentions-legales', function (req, res) {
  res.render('mentions-legales', { ok: "", no: "" });
});

app.post('/contact', function (req, res) {
  transporter.sendMail({
    from: req.body.nom + " " + req.body.prenom + " <" + req.body.email + ">",
    to: 'adrien.giraud0750@orange.fr',
    subject: 'Contact depuis le site Renov\' Fermetures',
    text: req.body.message
  }, function(err, info){
    if(err){
      req.flash('info', 'Une erreur est survenue.');
      res.redirect('/contact');
    }else{
      req.flash('info', 'Votre message a été envoyé.');
      res.redirect('/');
    }
  });
});

app.get('*', function(req, res){
  res.render('404', { ok: "", no: "" });
});

app.listen(3000, function () {
  console.log('http://localhost:3000');
});
